const db = require("../models");

module.exports.newPost = function(req,res) {
    res.render("compose");
}

module.exports.currentPost = function(req,res) {
    const requestedId = req.params.postId;
    const pageName = req.body.button;
  
    // console.log(pageName);
    // User.findOne({_id: req.user.id}, function(err,foundUser) {
    //   console.log(foundUser.posts);
    //   const foundPost = foundUser.posts.filter(post => {
    //     post._id == requestedId;
    //   });
    //   typeof(requestedId);
    //   console.log(requestedId);
    //   console.log(foundPost);
    //   if (err) {
    //     console.log(err);
    //   }else {
    //     res.render("post",{titleContent:foundPost[0].title, bodyContent:foundPost[0].content, postId:requestedId, img:foundPost[0].imgUrl});
    //   }
    //
    //
    // })
    db.Post.findOne({_id: requestedId}, function(err, foundPost){
        // if (err) {
        //   console.log(err);
        // }else {
          //console.log(foundPost);
          // const s = foundPost.updatedAt.split(" ")
          // console.log(s);
          if(pageName === "home"){
            res.render("allPosts",{
              titleContent:foundPost.title,
              bodyContent:foundPost.content,
              postId:requestedId,
              postDate:foundPost.updatedAt,
              creatorId:foundPost.creator,
              img:foundPost.imgUrl,
              comments:foundPost.comments,
              postedBy: foundPost.creatorName,
              likes: foundPost.likes,
              dislikes: foundPost.dislikes
            });
          }
          else if(pageName === "myBlogs"){
            res.render("post",{
              titleContent:foundPost.title,
              bodyContent:foundPost.content,
              postId:requestedId,
              postDate:foundPost.updatedAt,
              creatorId:foundPost.creator,
              img:foundPost.imgUrl,
              comments:foundPost.comments,
              postedBy: foundPost.creatorName,
              likes: foundPost.likes,
              dislikes: foundPost.dislikes
            });
          }
        //}
     });
}

module.exports.updatePost = function(req,res){
    const action = req.body.button;
    if(action === "edit"){
  
      db.Post.findOne({_id:req.body.id}, function(err, foundUser){
          if (err) {
            console.log(err);
          }else {
  
            res.render("Edit",{previousTitle:foundUser.title, previouscontent:foundUser.content, postId:foundUser._id, previousUrl:foundUser.imgUrl});
          }
       });
      }
    else if (action === "delete") {
  
      // User.findOneAndUpdate({_id:req.user.id}, {$pull: {posts: {_id: req.body.id}}}, function(err, foundUser) {
        // if(!err){
          db.Post.findOneAndDelete({_id: req.body.id}, function(err) {
            if(!err){
              res.redirect("/myBlogs/"+req.user.id);
            }
          });
        // }
      // });
  
      }
}

module.exports.edit = function(req,res) {

    db.Post.findOneAndUpdate({_id:req.body.id}, {$set: {title:req.body.title,imgUrl:req.body.img,content:req.body.post,}}, function(err) {
      if(err){
        res.send(err);
      }else{
        res.redirect("/myBlogs/"+req.user.id);
      }
    })
}

module.exports.comment = function(req,res) {
    const newComment = new db.Comment({
      name: req.user.username,
      content: req.body.comment
    });
  
    db.Post.findOne({_id: req.body.postId}, function(err,found) {
      found.comments.push(newComment);
      found.save(function(err) {
        res.redirect("/posts/"+req.body.postId);
      })
    });
}

module.exports.likeAndDislike = function(req,res) {
    if(req.body.type == "like"){
      db.Post.findOneAndUpdate({_id:req.body.postId}, {$inc: {likes:1}}, function(err) {
        if(err){
          res.send(err);
        }else{
          res.redirect("/posts/"+req.body.postId);
        }
      })
    }
    else{
      db.Post.findOneAndUpdate({_id:req.body.postId}, {$inc: {dislikes:1}}, function(err) {
        if(err){
          res.send(err);
        }else{
          res.redirect("/posts/"+req.body.postId);
        }
      })
    }
}

module.exports.compose = function(req,res) {

    const newPost = new db.Post({
      title:req.body.title,
      imgUrl:req.body.img,
      content:req.body.post,
      creator: req.user.id,
      creatorName: req.user.username,
      likes: 0,
      dislikes: 0
    });
  
    // User.findOne({_id: req.user.id}, function(err, found) {
    //     found.posts.push(newPost);
    //     found.save(function(err) {
    //       if(!err){
    //         // console.log(req.user.id);
    //       }
    //     });
    //   })
      newPost.save(function(err) {
        if(err){
          console.log(err);
        }else {
          // res.redirect("/myBlogs/"+req.user.id);
          res.redirect("/myProfile");
        }
      })
}