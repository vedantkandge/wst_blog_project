const mongoose = require('mongoose');
const commentSchema = new mongoose.Schema({
  name: String,
  content: String
});


const postSchema = new mongoose.Schema({
    title: {
      type: String,
      createIndexes: true
    },
    imgUrl:{
      type: String,
      createIndexes: true
    },
    content: {
      type: String,
      createIndexes: true
    },
    creator: {
      type: mongoose.Schema.ObjectId,
       ref: 'userSchema',
       createIndexes: true
     },
     creatorName: {
       type: String,
       createIndexes: true
     },
    comments:[commentSchema],
    likes: {
      type: Number,
      createIndexes: true
    },
    dislikes: {
      type: Number,
      createIndexes: true
    },
  },{
    timestamps: true
});


const Post = mongoose.model("Post",postSchema);
module.exports = Post;