const express = require("express");
const db = require("../models");
const passport = require('passport');
const router = express.Router({ mergeParams: true });
const { login, register } = require("../handlers/auth");
const { forgot, reset } = require("../handlers/forgot");
const {newPost, currentPost, updatePost, edit, comment, likeAndDislike, compose} = require("../handlers/pages");

const aboutContent = "Hac habitasse platea dictumst vestibulum rhoncus est pellentesque. Dictumst vestibulum rhoncus est pellentesque elit ullamcorper. Non diam phasellus vestibulum lorem sed. Platea dictumst quisque sagittis purus sit. Egestas sed sed risus pretium quam vulputate dignissim suspendisse. Mauris in aliquam sem fringilla. Semper risus in hendrerit gravida rutrum quisque non tellus orci. Amet massa vitae tortor condimentum lacinia quis vel eros. Enim ut tellus elementum sagittis   vitae. Mauris ultrices eros in cursus turpis massa tincidunt dui.";
const contactContent = "Scelerisque eleifend donec pretium vulputate sapien. Rhoncus urna neque viverra justo nec ultrices. Arcu dui vivamus arcu felis bibendum. Consectetur adipiscing elit duis tristique. Risus viverra adipiscing at in tellus integer feugiat. Sapien nec sagittis aliquam malesuada bibendum arcu vitae. Consequat interdum varius sit amet mattis. Iaculis nunc sed augue lacus. Interdum posuere lorem ipsum dolor sit amet consectetur adipiscing elit. Pulvinar elementum integer enim neque. Ultrices gravida dictum fusce ut placerat orci nulla. Mauris in aliquam sem fringilla ut morbi tincidunt. Tortor posuere ac ut consequat semper viverra nam libero.";

router.get("/",function(req,res) {
    res.render("start");
});

router.get("/about",function(req,res) {
    res.render("about",{aboutContent:aboutContent});
});
  
router.get("/contact",function(req,res) {
    res.render("contact",{contactContent:contactContent});
});

router.get("/forgot",function(req,res) {
    res.render("forgot")
});

router.get("/myBlog",function(req,res) {
    if(req.isAuthenticated()){
        res.redirect("/myBlogs/"+req.user.id);
    }else {
      res.redirect("/login");
    }
});

router.get("/myProfile",function(req,res){
  if(req.isAuthenticated()){
    db.User.findOne({_id: req.user.id}, function(err, foundUser){
      if(err){
        console.log(err);
      }
      else{
        db.Post.find({creator:req.user.id},function(err,foundPosts) {
          if(err){
          console.log(err);
        }else {
          if(foundPosts && foundUser){
            res.render("myProfile",{
              firstName:foundUser.firstName,
              lastName:foundUser.lastName,
              email: foundUser.username,
              phone: foundUser.phoneNumber,
              posts: foundPosts
            });
          }
        }
        }); 
      }
    })  
  }
  else {
    res.redirect("/login");
  }
})

router.get("/profile/:userId", function(req,res) {
  const requestedId = req.params.userId;
  // const userType = req.body.button;
  // console.log(requestedId);
  db.User.findOne({_id: requestedId}, function(err, foundUser){
    if(err){
      console.log(err);
    }
    else{
      db.Post.find({creator:requestedId},function(err,foundPosts) {
        if(err){
        console.log(err);
      }else {
        if(foundPosts && foundUser){
          res.render("profile",{
            firstName:foundUser.firstName,
            lastName:foundUser.lastName,
            email: foundUser.username,
            phone: foundUser.phoneNumber,
            posts: foundPosts
            // userType: userType
          });
        }
      }
      }); 
    }
  })
});

router.get("/auth/google",
    passport.authenticate("google", { scope: ["profile", "email"] }));

    router.get("/auth/google/callback",
    passport.authenticate("google", { failureRedirect: "/login" }),
    function(req, res) {
        // Successful authentication, redirect Home page.
        res.redirect("/home");
    });
    router.get("/login",function(req,res) {
    res.render("login")
});

router.get("/register",function(req,res) {
    res.render("register")
});

router.get("/home",function(req,res) {

    if(req.isAuthenticated()){
      db.Post.find(function(err, foundPost) {
          if(err){
            console.log(err);
          }else{
            res.render("home",
            {
              posts:foundPost,
              userId: req.user.id,
              email: req.user.username,
            });
          }
      })
    }else {
      res.redirect("/login");
    }
});

router.get("/logout",function(req,res) {
    req.logout();
    res.redirect("/");
});

router.get("/posts/:postId", function(req,res) {
    const requestedId = req.params.postId;
    const pageName = req.body.button;
    // console.log(requestedId);
    db.Post.findOne({_id: requestedId}, function(err, foundPost){
        // if (err) {
        //   console.log(err);
        // }else {
        
          // var m = date.getMonth()+1; 
          if(pageName === "home"){
            res.render("allPosts",{
              titleContent:foundPost.title,
              bodyContent:foundPost.content,
              postId:requestedId,
              postDate:foundPost.updatedAt,
              creatorId:foundPost.creator,
              img:foundPost.imgUrl,
              comments:foundPost.comments,
              postedBy: foundPost.creatorName,
              likes: foundPost.likes,
              dislikes: foundPost.dislikes
            });
          }
          else if(pageName === "myBlogs"){
            res.render("post",{
              titleContent:foundPost.title,
              bodyContent:foundPost.content,
              postId:requestedId,
              postDate:foundPost.updatedAt,
              creatorId:foundPost.creator,
              img:foundPost.imgUrl,
              comments:foundPost.comments,
              postedBy: foundPost.creatorName,
              likes: foundPost.likes,
              dislikes: foundPost.dislikes
            });
          }
          else{
            res.render("allPosts",{
              titleContent:foundPost.title,
              bodyContent:foundPost.content,
              postId:requestedId,
              postDate:foundPost.updatedAt,
              creatorId:foundPost.creator,
              img:foundPost.imgUrl,
              comments:foundPost.comments,
              postedBy: foundPost.creatorName,
              likes: foundPost.likes,
              dislikes: foundPost.dislikes
            });
          }
        //}
     });
});

router.get("/reset/:token",function(req,res) {
    db.User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
      if (!user) {
        req.flash('error', 'Password reset token is invalid or has expired.');
        return res.redirect('/forgot');
      }
      res.render('reset', {token: req.params.token});
    });
});

router.get("/myBlogs/:userId",function(req,res) {
    const requestedUserId = req.params.userId;
    // console.log(requestedUserId);
    db.Post.find({creator:requestedUserId},function(err,foundPosts) {
      // console.log(foundPosts);
      if(err){
      console.log(err);
    }else {
      if(foundPosts){
        res.render("myBlogs", {posts: foundPosts, userId:requestedUserId})
      }
    }
  });
  //   User.findOne({_id: requestedUserId}, function(err, found) {
  //
  // });
});


router.post("/forgot",forgot);
router.post("/reset/:token",reset);
router.post("/posts/:postId",currentPost);
router.post("/register",register);
router.post("/login",login);
router.post("/new",newPost);
router.post("/post",updatePost);
router.post("/edit",edit);
router.post("/comment",comment);
router.post("/likeAndDislike",likeAndDislike);
router.post("/compose",compose);

module.exports = router;
